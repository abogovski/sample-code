#include "ac_ppm.h"
#include <cstdlib>

acPPM::acPPM(int init_aggr) :
    aggr(init_aggr),
    filled(0),
    start_table(init_aggr)
{
    for (int i = 0; i < AC_CODE_BOUND; ++i)
        for (int j = 0; j < AC_CODE_BOUND; ++j)
            tables[i][j] = NULL;
}

acPPM::~acPPM()
{
    for (int i = 0; i < AC_CODE_BOUND; ++i)
        for (int j = 0; j < AC_CODE_BOUND; ++j)
            if (tables[i][j] != NULL)
                delete tables[i][j];
}

acCodeTable *acPPM::table()
{
    return filled >= 2 ? tables[prev[0]][prev[1]] : &start_table;
}

void acPPM::update(uint16_t symbol)
{
    switch (filled) {
    case 0:
        prev[filled++] = symbol;
        break;
    case 1:
        prev[filled++] = symbol;
        tables[prev[0]][prev[1]] = new acCodeTable(aggr);
        break;
    default:
        table()->update(table()->idx(symbol));
        prev[0] = prev[1];
        prev[1] = symbol;

        if (table() == NULL)
            tables[prev[0]][prev[1]] = new acCodeTable(aggr);
    }
}
