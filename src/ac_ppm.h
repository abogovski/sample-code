#ifndef AC_PPM
#define AC_PPM

#include "ac_code_table.h"

class acPPM
{
public:
    acPPM(int init_aggr);
    ~acPPM();

    acCodeTable *table();
    void update(uint16_t symbol); // idx in table();

private:
    int filled;
    uint16_t prev[2];
    int aggr;
    acCodeTable *tables[AC_CODE_BOUND][AC_CODE_BOUND];
    acCodeTable start_table;
};

#endif // AC_PPM