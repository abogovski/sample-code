#include "ac_code_table.h"
#include <algorithm>
#include <cstdlib>
#include <cassert>

acCodeTable::acCodeTable(uint16_t init_aggr) :
    aggr(init_aggr)
{
	assert(AC_CODE_NORM >= AC_CODE_BOUND);

	uint32_t init_width = AC_CODE_NORM / AC_CODE_BOUND;
	uint32_t rem = AC_CODE_NORM % AC_CODE_BOUND;

	for (uint32_t i = 0; i < AC_CODE_BOUND; ++i) {
		syms[i] = i;
		widths[i] = init_width + (i < rem);
	}

	bases[0] = 0;
	for (uint32_t i = 1; i < AC_CODE_BOUND; ++i)
		bases[i] = bases[i - 1] + widths[i - 1];
	
    normalize();
}

uint32_t acCodeTable::max()
{
	return bases[AC_CODE_BOUND - 1] + widths[AC_CODE_BOUND - 1];
}

uint32_t acCodeTable::base(uint16_t symbol_idx)
{
	assert(symbol_idx < AC_CODE_BOUND);
	return bases[symbol_idx];
}

uint32_t acCodeTable::width(uint16_t symbol_idx)
{
	assert(symbol_idx < AC_CODE_BOUND);
	return widths[symbol_idx];
}

uint16_t acCodeTable::idx(uint16_t symbol)
{
	assert(symbol < AC_CODE_BOUND);

	for (uint32_t i = 0; i < AC_CODE_BOUND; ++i)
		if (syms[i] == symbol)
			return i;

	abort();
}

uint16_t acCodeTable::symbol(uint16_t idx)
{
	assert(idx < AC_CODE_BOUND);
	return syms[idx];
}

static bool width_compar(uint32_t a, uint32_t b)
{
	return a > b;
}

void acCodeTable::update(uint16_t symbol_idx)
{
	assert(symbol_idx < AC_CODE_BOUND);

	widths[symbol_idx] += aggr;
	for (uint32_t i = symbol_idx + 1; i < AC_CODE_BOUND; ++i)
		bases[i] += aggr;

	uint32_t sym_value = syms[symbol_idx];
	uint32_t sym_width = widths[symbol_idx];

	// restore order
	uint32_t *place = std::upper_bound(widths, widths + symbol_idx, sym_width, width_compar);
	uint32_t w = *place;
	uint32_t new_idx = place - widths;
	
	for (uint32_t i = symbol_idx; i > new_idx; --i) {
		syms[i] = syms[i - 1];
		widths[i] = widths[i - 1];
		bases[i] = bases[i - 1] + sym_width - (i != new_idx + 1) * w;
	}
	syms[new_idx] = sym_value;
	widths[new_idx] = sym_width;

    for (uint32_t i = 0; i < AC_CODE_BOUND; ++i)
        if (!widths[i])
            abort();

	// normalize will also rebuild bases array
    if (normalize())
        return;

    for (uint32_t i = new_idx + 1; i < AC_CODE_BOUND; ++i)
        bases[i] = bases[i - 1] + widths[i - 1];
}

bool acCodeTable::normalize()
{
	uint32_t table_max = max();

	long d = table_max - AC_CODE_NORM;
	if (abs(d) * AC_CODE_NORM_REL_ERR_DENOM <= AC_CODE_NORM_REL_ERR_NOM * AC_CODE_NORM)
		return false;

	for (uint32_t i = 0; i < AC_CODE_BOUND; ++i) {
		widths[i] = (uint64_t)widths[i] * AC_CODE_NORM / table_max;
		widths[i] += !widths[i];
	}

    // TODO: check what if rem is big

    for (uint32_t i = 1; i < AC_CODE_BOUND; ++i)
        bases[i] = bases[i - 1] + widths[i - 1];

    for (uint32_t i = 0; i < AC_CODE_BOUND; ++i)
        if (!widths[i])
            abort();

    return true;
}