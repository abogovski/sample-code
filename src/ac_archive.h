#ifndef AC_ARCHIVE
#define AC_ARCHIVE

#include "ac_code_table.h"
#include <vector>

//////////////////////////////////////////////////////////////////////////
// Archive
//////////////////////////////////////////////////////////////////////////

struct acArchive
{
	friend class acArchiveReader;
	friend class acArchiveWriter;

public:
	acArchive();

	std::vector<bool> bits;
	uint32_t syms_cnt;

private:
	bool in_use;
};

int ac_save_archive(acArchive *src, const char *fname);
int ac_load_archive(acArchive *dst, const char *fname);

//////////////////////////////////////////////////////////////////////////
// Archive Writer
//////////////////////////////////////////////////////////////////////////

class acArchiveWriter
{
public:
	acArchiveWriter(acArchive *archive);
	~acArchiveWriter();

	int write(acCodeTable *table, uint16_t symbol_idx);
	acArchive *archive();

private:
	void followBits(int bit);

	acArchive *arch;
	uint32_t low;
	uint32_t high;
	size_t bits_to_follow;
};

//////////////////////////////////////////////////////////////////////////
// Archive Reader
//////////////////////////////////////////////////////////////////////////

class acArchiveReader
{
public:
	acArchiveReader(acArchive *archive);
	~acArchiveReader();

	int read(acCodeTable *table, uint16_t *symbol_idx);
	acArchive *archive();

private:
	int nextBit(); // returns next bit or 0 if no bits left

	acArchive *arch;
	uint32_t low;
	uint32_t high;
	uint32_t value;
	size_t symbols_read;
	size_t bits_read;
};

#endif // AC_ARCHIVE