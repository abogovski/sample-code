#ifndef ARCHIVER_INCLUDED
#define ARCHIVER_INCLUDED

#include "ac_archive.h"
#include "ac_ppm.h"

enum COMPR_MODE
{
    COMPR_MODE_PURE,
    COMPR_MODE_PPM
};

void compress(acArchive *arch, const char *input, int aggr);
void compress_ppm(acArchive *arch, const char *input, int aggr);
void decompress(acArchive *arch, const char *output, int aggr);
void decompress_ppm(acArchive *arch, const char *output, int aggr);

void fcompress(const char *input, const char *output, COMPR_MODE mode);
void fdecompress(const char *input, const char *output);

#endif // ARCHIVER_INCLUDED