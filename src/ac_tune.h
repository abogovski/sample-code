#ifndef AC_TUNE_INCLUDED
#define AC_TINE_INCLUDED

#include <cstdint>
#include <climits>

enum { AC_TEXT_AGGR = 8 };
enum { AC_EXE_AGGR = 52 };

enum { AC_PPM_AGGR_LOW = 120 };
enum { AC_PPM_AGGR_HIGH = 397 };

enum { AC_CODE_BOUND = UINT8_MAX + 1};

enum { AC_SPAN_NORM = UINT16_MAX };
enum { AC_VALUE_LEN = 16 };

enum { AC_CODE_NORM_REL_ERR_NOM = 1 }; 
enum { AC_CODE_NORM_REL_ERR_DENOM = 5 };
enum { AC_CODE_NORM = UINT16_MAX / 8 };

#endif // AC_TUNE_INCLUDED 