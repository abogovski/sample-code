#include "ac_archive.h"
#include <cassert>

static const uint32_t first_qtr = (AC_SPAN_NORM + 1) / 4;
static const uint32_t half = first_qtr * 2;
static const uint32_t third_qtr = first_qtr * 3;

//////////////////////////////////////////////////////////////////////////
// Archive
//////////////////////////////////////////////////////////////////////////

acArchive::acArchive() :
	bits(),
	syms_cnt(0),
	in_use(false)
{}

int ac_save_archive(acArchive *src, const char *fname)
{
    FILE *file = fopen(fname, "wb");
    if (file == NULL)
        return -1;

    fwrite(&src->syms_cnt, sizeof(src->syms_cnt), 1, file);
    size_t size = src->bits.size();
    size_t i = 0;
    int byte;
    for (; i + CHAR_BIT <= size; i += CHAR_BIT) {
        byte = 0;
        for (int j = 0; j < CHAR_BIT; ++j)
            byte |= !!src->bits[i + j] << j;
        fputc(byte, file);
    }

    byte = 0;
    for (size_t j = 0; j < size - i; ++j)
        byte |= !!src->bits[i + j] << j;
    fputc(byte, file);

    fclose(file);
    return 0;
}

int ac_load_archive(acArchive *dst, const char *fname)
{
    FILE *file = fopen(fname, "rb");
    if (file == NULL)
        return -1;

    if (fread(&dst->syms_cnt, sizeof(dst->syms_cnt), 1, file) != 1) {
        fclose(file);
        return -1;
    }

    dst->bits.clear();
    for (int c; (c = fgetc(file)) != EOF; )
        for (int i = 0; i < CHAR_BIT; ++i) 
            dst->bits.push_back(!!(c & (1 << i)));

    while (dst->bits.size() && !dst->bits.back())
        dst->bits.pop_back();

    fclose(file);
    return 0;
}

//////////////////////////////////////////////////////////////////////////
// Archive Writer
//////////////////////////////////////////////////////////////////////////

acArchiveWriter::acArchiveWriter(acArchive *archive) :
	arch(archive),
	low(0),
	high(AC_SPAN_NORM),
	bits_to_follow(0)
{
	assert(!arch->in_use);
	arch->in_use = true;
}

acArchiveWriter::~acArchiveWriter()
{
   arch->bits.push_back(1);
   arch->in_use = false;
}

acArchive *acArchiveWriter::archive()
{
	return arch;
}

void acArchiveWriter::followBits(int bit)
{
	arch->bits.push_back(!!bit);
	for (size_t i = 0; i < bits_to_follow; ++i)
		arch->bits.push_back(!bit);
	bits_to_follow = 0;
}

int acArchiveWriter::write(acCodeTable *table, uint16_t symbol_idx)
{
	assert(symbol_idx < AC_CODE_BOUND);
	uint32_t old_bitcnt = arch->bits.size();

	uint32_t symb_low = table->base(symbol_idx);
	uint32_t symb_high = symb_low + table->width(symbol_idx);

	assert((high - low + 1) >= table->max());
	uint32_t l = low + symb_low * (high - low + 1) / table->max();
	uint32_t h = low + symb_high * (high - low + 1) / table->max() - 1;

	for (;;) {
		if (h < half)
			followBits(0);
		else if (l >= half) {
			followBits(1);
			l -= half;
			h -= half;
		}
		else if (l >= first_qtr && h < third_qtr) {
			bits_to_follow++;
			l -= first_qtr;
			h -= first_qtr;
		}
		else
	 		break;
		l += l;
		h += h + 1;
	}

	low = l;
	high = h;
	arch->syms_cnt++;

	return arch->bits.size() - old_bitcnt;
}

//////////////////////////////////////////////////////////////////////////
// Archive Reader
//////////////////////////////////////////////////////////////////////////

acArchiveReader::acArchiveReader(acArchive *archive) :
	arch(archive),
	value(0),
	low(0),
	high(AC_SPAN_NORM),
	symbols_read(0),
	bits_read(0)
{
	assert(!arch->in_use);
	arch->in_use = true;

	for (int i = 0; i < AC_VALUE_LEN; ++i)
		value = value * 2 + nextBit();
}

acArchiveReader::~acArchiveReader()
{
	arch->in_use = false;
}

acArchive *acArchiveReader::archive()
{
	return arch;
}

int acArchiveReader::nextBit()
{
    if (bits_read == arch->bits.size())
        return 0;

	return !!arch->bits[bits_read++];
}

int acArchiveReader::read(acCodeTable *table, uint16_t *symbol_idx)
{
	if (symbols_read == arch->syms_cnt)
		return -1;

	uint32_t old_bits_read = bits_read;

	// find symbol
	uint32_t i = 0;
	uint32_t freq = ((value - low + 1) * table->max() - 1) / (high - low + 1);
	while (table->base(i) + table->width(i) <= freq)
		++i;
	*symbol_idx = i;

	// update [low; high)
	uint32_t symb_low = table->base(i);
	uint32_t symb_high = symb_low + table->width(i);

	assert((high - low + 1) >= table->max());
	uint32_t l = low + symb_low * (high - low + 1) / table->max();
	uint32_t h = low + symb_high * (high - low + 1) / table->max() - 1;
	
	// update value and [low; high)
	for (;;) {
		if (h < half) {
			// do nothing
		}
		else if (l >= half){
			l -= half;
			h -= half;
			value -= half;
		}
		else if (l >= first_qtr && h < third_qtr) {
			l -= first_qtr;
			h -= first_qtr;
			value -= first_qtr;
		}
		else
			break;

		l += l;
		h += h + 1;
		value += value + nextBit();
	}

	low = l;
	high = h;
	++symbols_read;

	return bits_read - old_bits_read;
}