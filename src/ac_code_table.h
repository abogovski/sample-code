#ifndef AC_CODE_TABLE
#define AC_CODE_TABLE

#include "ac_tune.h"
#include <cstdint>

class acCodeTable
{
public:
    struct Span
	{
		uint32_t low;
		uint32_t high;
	};

    uint32_t aggr; // used in update()

    acCodeTable(uint16_t aggr);

    uint32_t max(); // max high bound

	uint16_t idx(uint16_t symbol);
	uint16_t symbol(uint16_t idx);

	uint32_t base(uint16_t symbol_idx);
	uint32_t width(uint16_t symbol_idx);

	void update(uint16_t symbol_idx);
	bool normalize(); // precision can be lost
	     
private:
	uint16_t syms[AC_CODE_BOUND];
	uint32_t bases[AC_CODE_BOUND];
	uint32_t widths[AC_CODE_BOUND];
};

#endif // AC_CODE_TABLE