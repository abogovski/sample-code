#include "archiver.h"

void compress(acArchive *arch, const char *input, int aggr)
{
    acArchiveWriter wrtr(arch);
    acCodeTable table(aggr);

    FILE *file = fopen(input, "rb");
    if (file == NULL) {
        printf("ERR input file not found.\n");
        abort();
    }

    for (int c, k = 0; (c = fgetc(file)) != EOF; ++k) {
        uint16_t i = table.idx(c);
        wrtr.write(&table, i);
        table.update(i);

        if (k % (512 * 1024) == 0)
            printf("%6d KB Encoded\n", k / 1024);
    }

    fclose(file);
}

void compress_ppm(acArchive *arch, const char *input, int aggr)
{
    acArchiveWriter wrtr(arch);
    acPPM ppm(aggr);

    FILE *file = fopen(input, "rb");
    if (file == NULL) {
        printf("ERR input file not found.\n");
        abort();
    }

    for (int c, k = 0; (c = fgetc(file)) != EOF; ++k) {
        wrtr.write(ppm.table(), ppm.table()->idx(c));
        ppm.update(c);

        if (k % (512 * 1024) == 0)
            printf("%6d KB Encoded\n", k / 1024);
    }

    fclose(file);
}

void decompress(acArchive *arch, const char *output, int aggr)
{
    acArchiveReader rdr(arch);
    acCodeTable table(aggr);

    FILE *file = fopen(output, "wb");
    if (file == NULL) {
        printf("ERR output file not found.\n");
        abort();
    }

    uint16_t i;
    for (int j = 0; rdr.read(&table, &i) != -1; ++j) {
        fputc(table.symbol(i), file);
        table.update(i);

        if (j % (512 * 1024) == 0)
            printf("%6d KB Decoded\n", j / 1024);
    }

    fclose(file);
}

void decompress_ppm(acArchive *arch, const char *output, int aggr)
{
    acArchiveReader rdr(arch);
    acPPM ppm(aggr);

    FILE *file = fopen(output, "wb");
    if (file == NULL) {
        printf("ERR output file not found.\n");
        abort();
    }

    uint16_t i;
    for (int j = 0; rdr.read(ppm.table(), &i) != -1; ++j) {
        int c = ppm.table()->symbol(i);
        fputc(c, file);
        ppm.update(c);

        if (j % (512 * 1024) == 0)
            printf("%6d KB Decoded\n", j / 1024);
    }

    fclose(file);
}

static int get_min_archive(acArchive *arch, int cnt)
{
    int min = 0;
    for (int i = 1; i < cnt; ++i)
        if (arch[i].bits.size() < arch[min].bits.size())
            min = i;

    return min;
}

void fcompress(const char *ifname, const char *ofname, COMPR_MODE mode)
{
    acArchive arch1, arch2;
    if (mode == COMPR_MODE_PURE) {
        printf("Encoding attempt (1 of 2): as text.\n");
        compress(&arch1, ifname, AC_TEXT_AGGR);

        printf("Encoding attempt (2 of 2): as exe file.\n");
        compress(&arch2, ifname, AC_EXE_AGGR);
    }
    else if (mode == COMPR_MODE_PPM) {
        printf("Encoding attempt (1 of 2): with low aggression.\n");
        compress_ppm(&arch1, ifname, AC_PPM_AGGR_LOW);

        printf("Encoding attempt (2 of 2): with high aggression.\n");
        compress_ppm(&arch2, ifname, AC_PPM_AGGR_HIGH);
    }
    else
        abort();

    acArchive *arch = &arch1;
    if (arch1.bits.size() > arch2.bits.size()) {
         arch = &arch2;
         arch2.syms_cnt |= 1 << 30;
    }

    if (mode == COMPR_MODE_PPM)
        arch->syms_cnt |= 1 << 31;

    printf("Saving archive.\n");
    if (-1 == ac_save_archive(arch, ofname)) {
        printf("Cannot save archive.\n");
        abort();
    }
}

void fdecompress(const char *ifname, const char *ofname)
{
    acArchive arch;

    printf("Loading archive.\n");
    if (-1 == ac_load_archive(&arch, ifname)) {
        printf("Cannot load archive.\n");
        abort();
    }

    bool use_ppm = !!(arch.syms_cnt & (1 << 31));
    arch.syms_cnt &= ~(1 << 31);

    printf("Decompressing archive.\n");
    if (use_ppm) {
        int aggr = AC_PPM_AGGR_LOW;
        if (arch.syms_cnt & (1 << 30)) {
            aggr = AC_PPM_AGGR_HIGH;
            arch.syms_cnt &= ~(1 << 30);
        }
        decompress_ppm(&arch, ofname, aggr);
    }
    else {
        int aggr = AC_TEXT_AGGR;
        if (arch.syms_cnt & (1 << 30)) {
            aggr = AC_EXE_AGGR;
            arch.syms_cnt &= ~(1 << 30);
        }
        decompress(&arch, ofname, aggr);
    }
}
