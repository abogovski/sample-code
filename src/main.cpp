#include "archiver.h"
#include <cstdio>
#include <cassert>

void fcompar(const char *f1, const char *f2) // for tests
{
    FILE *file1 = fopen(f1, "rb");
    FILE *file2 = fopen(f2, "rb");

    assert(file1 && file2);

    int c1, c2;
    
    for (int i = 0; ; ++i) {
        c1 = fgetc(file1);
        c2 = fgetc(file2);
        if (c1 != c2) {
            printf("Files differ at pos %d\n", i);
            fflush(stdout);
            return;
        }
        if (c1 == EOF)
            break;
    }

    printf("Files match.\n");
}

void test(const char *input, const char *archive, const char *output)
{
    // for testing
    fcompar(input, output);
    acArchive arch;

    printf("Loading archive.\n");
    if (-1 == ac_load_archive(&arch, archive)) {
        printf("Cannot load archive.\n");
        abort();
    }

    size_t s1 = arch.syms_cnt & ~(1 << 30 | 1 << 31);
    size_t s2 = (arch.bits.size() + 4) / 8;
    
    printf("Uncompressed size: %d\n", s1);
    printf("Compressed size: %d\n", s2);
    if (!s1)
        return;
    printf("Ratio: %.3lf\n", 100.0 * s2 / s1);
}

int main(int argc, char *argv[])
{
    if (!strcmp("c", argv[1])) {
        COMPR_MODE mode = COMPR_MODE_PURE;
        if (!strcmp("ppm", argv[4]))
            mode = COMPR_MODE_PPM;
        fcompress(argv[2], argv[3], mode);
    }
    else if (!strcmp("d", argv[1]))
        fdecompress(argv[2], argv[3]);
    else if (!strcmp("t", argv[1]))
        test(argv[2], argv[3], argv[4]);
    else {
        printf("First argument \'%s\' is invalid.\n", argv[1]);
        abort();
    }
       
    return 0;
}

